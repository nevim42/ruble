const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const ROOT_DIR = path.resolve(__dirname, '..');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');

/**
 * Entry point of this script thats will be executed.
 *
 * Cleanup all build code
 */
const main = () => {
  if (!fs.existsSync(DIST_DIR)) {
    // Nothing to clean
    return;
  }

  let files = fs.readdirSync(DIST_DIR);
  files = files.filter((file) => {
    if (file === '.') return false;
    if (file === '..') return false;
    if (file === 'rust.d.ts') return false;

    return true;
  });

  for (const file of files) {
    rimraf.sync(path.resolve(DIST_DIR, file));
  }
};

main();
