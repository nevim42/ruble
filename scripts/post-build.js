/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');

const ROOT_DIR = path.resolve(__dirname, '..');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const INDEX_FILE = path.resolve(DIST_DIR, 'index.js');
const INDEX_TYPES_FILE = path.resolve(DIST_DIR, 'index.d.ts');

const fix = (file) => {
  if (!fs.existsSync(file)) {
    console.error(`File ${file} doesn't exists! Try to build TS first!`);
    return false;
  }

  const content = fs.readFileSync(file).toString();
  if (!content || content.length < 1) {
    console.error(`File ${file} is corrupted or empty!`);
    return false;
  }

  const what = `../dist/rust`;
  const to = `./rust`;
  const editedContent = content.replace(what, to);

  if (editedContent === content) {
    console.log('No imports was changed!');
    return true;
  }

  fs.writeFileSync(file, editedContent, { flag: 'w+' });
  return true;
};

/**
 * Entry point of this script thats will be executed.
 *
 * Main reason for this script is to resolve import to rust
 * bindings, because of TypeScript is totally useless in this
 * situation...
 */
const main = () => {
  let success = fix(INDEX_FILE);
  if (!success) process.exit(1);

  success = fix(INDEX_TYPES_FILE);
  if (!success) process.exit(1);
};

main();
