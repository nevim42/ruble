use crate::model::ToJsArray;
use ble::BleModule;
use neon::prelude::*;
use once_cell::sync::Lazy;
use std::pin::Pin;
use std::sync::Arc;
use tokio::runtime::Runtime;
use utils::central_event_to_js_event;

mod ble;
mod error;
mod model;
mod utils;

// Define number of allowed threads for this library to use
const WORKER_THREADS: usize = 2;

#[allow(dead_code)]
static RUNTIME: Lazy<Runtime> = Lazy::new(|| {
    tokio::runtime::Builder::new_multi_thread()
        .worker_threads(WORKER_THREADS)
        .enable_all()
        .build()
        .unwrap()
});

#[allow(dead_code)]
static BLE_MODULE: Lazy<Pin<Arc<BleModule>>> = Lazy::new(|| {
    let module = BleModule::new();
    Arc::pin(module)
});

fn init(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.init()))
        .promise(move |mut cx, _| Ok(cx.undefined()));

    Ok(promise)
}

fn start_scan(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.start_scan()))
        .promise(move |mut cx, _| Ok(cx.undefined()));

    Ok(promise)
}

fn stop_scan(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.stop_scan()))
        .promise(move |mut cx, _| Ok(cx.undefined()));

    Ok(promise)
}

fn peripherals(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.peripherals()))
        .promise(move |mut cx, res| {
            let res = res.unwrap();
            res.to_array(&mut cx)
        });

    Ok(promise)
}

fn connect(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.connect(&name)))
        .promise(move |mut cx, success| {
            let success = success.unwrap();
            Ok(cx.boolean(success))
        });

    Ok(promise)
}

fn disconnect(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.disconnect()))
        .promise(move |mut cx, _| Ok(cx.undefined()));

    Ok(promise)
}

fn discover(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.discover(&name)))
        .promise(move |mut cx, res| {
            res.unwrap();
            Ok(cx.undefined())
        });

    Ok(promise)
}

fn services(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.services(&name)))
        .promise(move |mut cx, res| {
            let res = res.unwrap();
            res.to_array(&mut cx)
        });

    Ok(promise)
}

fn characteristics(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.characteristics(&name)))
        .promise(move |mut cx, res| {
            let res = res.unwrap();
            res.to_array(&mut cx)
        });

    Ok(promise)
}

fn write(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let char_uuid = cx.argument::<JsString>(1)?.value(&mut cx);
    let data = cx.argument::<JsArray>(2)?.to_vec(&mut cx)?;
    let write_type = match cx.argument::<JsNumber>(3)?.value(&mut cx) as u8 {
        1 => btleplug::api::WriteType::WithResponse,
        _ => btleplug::api::WriteType::WithoutResponse,
    };

    let data: Vec<u8> = data
        .iter()
        .map(|val| {
            let num: Handle<JsNumber> = val.downcast(&mut cx).unwrap();
            let num = num.value(&mut cx);
            num as u8
        })
        .collect();

    let promise = cx
        .task(move || {
            RUNTIME
                .handle()
                .block_on(ble.write(&name, &char_uuid, &data, write_type))
        })
        .promise(move |mut cx, res| {
            res.unwrap();
            Ok(cx.undefined())
        });

    Ok(promise)
}

fn notify(mut cx: FunctionContext) -> JsResult<JsPromise> {
    let ble = BLE_MODULE.clone();
    let name = cx.argument::<JsString>(0)?.value(&mut cx);
    let char_uuid = cx.argument::<JsString>(1)?.value(&mut cx);
    let promise = cx
        .task(move || RUNTIME.handle().block_on(ble.notify(&name, &char_uuid)))
        .promise(move |mut cx, res| {
            res.unwrap();
            Ok(cx.undefined())
        });

    Ok(promise)
}

fn subscribe(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let ble = BLE_MODULE.clone();
    let callback = cx.argument::<JsFunction>(0)?.root(&mut cx);
    let channel = cx.channel();

    RUNTIME.handle().spawn(async move {
        let event = ble.receive_event().await.unwrap();
        let peripherals = ble.peripherals().await.map_or_else(|_| None, |p| Some(p));

        channel.send(move |mut cx| {
            let callback = callback.into_inner(&mut cx);
            let this = cx.undefined();

            let evnt = central_event_to_js_event(&mut cx, event, peripherals)?;
            callback.call(&mut cx, this, vec![evnt.upcast()])?;
            Ok(())
        });
    });

    Ok(cx.undefined())
}

fn data(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let ble = BLE_MODULE.clone();
    let callback = cx.argument::<JsFunction>(0)?.root(&mut cx);
    let channel = cx.channel();

    RUNTIME.handle().spawn(async move {
        let val = loop {
            let val = ble.receive_data().await;
            if val.is_ok() {
                break val.unwrap();
            }

            tokio::time::sleep(tokio::time::Duration::from_millis(2)).await;
        };

        channel.send(move |mut cx| {
            let callback = callback.into_inner(&mut cx);
            let this = cx.undefined();
            let value = if val.is_none() {
                cx.null().upcast()
            } else {
                let val = val.unwrap();
                let arr = JsArray::new(&mut cx, val.value.len() as u32);

                for (i, v) in val.value.iter().enumerate() {
                    let num = cx.number(v.clone());
                    arr.set(&mut cx, i as u32, num)?;
                }
                arr.upcast()
            };

            callback.call(&mut cx, this, vec![value])?;
            Ok(())
        });
    });

    Ok(cx.undefined())
}

#[neon::main]
fn main(mut cx: ModuleContext) -> NeonResult<()> {
    // Currently used os
    let os = std::env::consts::OS;
    println!("Currently running OS: {}", os);

    // Bind all function to the JavaScript lang
    cx.export_function("init", init)?;
    cx.export_function("start_scan", start_scan)?;
    cx.export_function("stop_scan", stop_scan)?;
    cx.export_function("peripherals", peripherals)?;
    cx.export_function("connect", connect)?;
    cx.export_function("disconnect", disconnect)?;

    // Services and Characteristic
    cx.export_function("discover", discover)?;
    cx.export_function("services", services)?;
    cx.export_function("characteristics", characteristics)?;
    cx.export_function("write", write)?;
    cx.export_function("notify", notify)?;

    // Subscription
    cx.export_function("catch_event", subscribe)?;
    cx.export_function("catch_data", data)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn it_works() {
        // Just an example of test in Rust
        assert_eq!(1 + 1, 2);
    }
}
