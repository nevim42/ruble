#![allow(dead_code)]
use crate::utils::get_uuid_from_peripheral;
use btleplug::api::CharPropFlags;
use btleplug::api::Peripheral as _;
use btleplug::platform::PeripheralId;
use neon::prelude::JsArray;
use neon::prelude::{Context, JsObject, JsResult, Object};

pub trait ToJsObject {
    fn to_object<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsObject>;
}

pub trait ToJsArray {
    fn to_array<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsArray>;
}

#[derive(Clone, Debug)]
pub struct Peripheral {
    pub id: String,
    pub name: Option<String>,
    pub peripheral_id: PeripheralId,
}

#[derive(Clone, Debug)]
pub struct Service {
    pub uuid: String,
    pub primary: bool,
}

#[derive(Clone)]
pub struct CharacteristicPropertiesFlags {
    pub broadcast: bool,
    pub read: bool,
    pub write_without_response: bool,
    pub write: bool,
    pub notify: bool,
    pub indicate: bool,
    pub authenticated_signed_writes: bool,
    pub extended_properties: bool,
}

#[derive(Clone, Debug)]
pub struct Characteristic {
    pub uuid: String,
    pub service_uuid: String,
    pub properties: CharacteristicPropertiesFlags,
}

impl Peripheral {
    pub async fn from(peripheral: &btleplug::platform::Peripheral) -> Self {
        let id = get_uuid_from_peripheral(&peripheral);
        let peripheral_id = peripheral.id();

        let name = async {
            let props = peripheral.properties().await;
            if props.is_err() {
                return None;
            }
            let props = props.unwrap();

            if props.is_none() {
                return None;
            }
            let props = props.unwrap();

            if props.local_name.is_none() {
                return None;
            }
            props.local_name.clone()
        }
        .await;

        Self {
            id,
            name,
            peripheral_id,
        }
    }
}

impl ToJsObject for Peripheral {
    fn to_object<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsObject> {
        let res = cx.empty_object();

        let id = cx.string(self.id.clone());
        res.set(cx, "id", id)?;

        let name = self
            .name
            .as_ref()
            .map_or_else(|| String::from("Unknown"), |n| n.clone());
        let name = cx.string(name);
        res.set(cx, "name", name)?;

        Ok(res)
    }
}

impl From<&btleplug::api::Service> for Service {
    fn from(service: &btleplug::api::Service) -> Self {
        let uuid = service.uuid.to_string();
        let primary = service.primary;

        Self { uuid, primary }
    }
}

impl ToJsObject for Service {
    fn to_object<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsObject> {
        let res = cx.empty_object();

        let uuid = cx.string(self.uuid.clone());
        res.set(cx, "uuid", uuid)?;

        let primary = cx.boolean(self.primary);
        res.set(cx, "primary", primary)?;

        Ok(res)
    }
}

impl From<CharPropFlags> for CharacteristicPropertiesFlags {
    fn from(flags: CharPropFlags) -> Self {
        let broadcast = flags.contains(CharPropFlags::BROADCAST);
        let read = flags.contains(CharPropFlags::READ);
        let write_without_response = flags.contains(CharPropFlags::WRITE_WITHOUT_RESPONSE);
        let write = flags.contains(CharPropFlags::WRITE);
        let notify = flags.contains(CharPropFlags::NOTIFY);
        let indicate = flags.contains(CharPropFlags::INDICATE);
        let authenticated_signed_writes =
            flags.contains(CharPropFlags::AUTHENTICATED_SIGNED_WRITES);
        let extended_properties = flags.contains(CharPropFlags::EXTENDED_PROPERTIES);

        Self {
            broadcast,
            read,
            write_without_response,
            write,
            notify,
            indicate,
            authenticated_signed_writes,
            extended_properties,
        }
    }
}

impl std::fmt::Display for CharacteristicPropertiesFlags {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut str = String::new();

        if self.broadcast {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}BROADCAST", str);
        }

        if self.read {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}READ", str);
        }

        if self.write_without_response {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}WRITE_WITHOUT_RESPONSE", str);
        }

        if self.write {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}WRITE", str);
        }

        if self.notify {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}NOTIFY", str);
        }

        if self.indicate {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}INDICATE", str);
        }

        if self.authenticated_signed_writes {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}AUTHENTICATED_SIGNED_WRITES", str);
        }

        if self.extended_properties {
            if str.len() > 0 {
                str = format!("{} | ", str);
            }
            str = format!("{}EXTENDED_PROPERTIES", str);
        }

        write!(f, "{}", str)
    }
}

impl std::fmt::Debug for CharacteristicPropertiesFlags {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl ToJsObject for CharacteristicPropertiesFlags {
    fn to_object<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsObject> {
        let res = cx.empty_object();

        let flag = cx.boolean(self.broadcast);
        res.set(cx, "broadcast", flag)?;

        let flag = cx.boolean(self.read);
        res.set(cx, "read", flag)?;

        let flag = cx.boolean(self.write_without_response);
        res.set(cx, "write_without_response", flag)?;

        let flag = cx.boolean(self.write);
        res.set(cx, "write", flag)?;

        let flag = cx.boolean(self.notify);
        res.set(cx, "notify", flag)?;

        let flag = cx.boolean(self.indicate);
        res.set(cx, "indicate", flag)?;

        let flag = cx.boolean(self.authenticated_signed_writes);
        res.set(cx, "authenticated_signed_writes", flag)?;

        let flag = cx.boolean(self.extended_properties);
        res.set(cx, "extended_properties", flag)?;

        Ok(res)
    }
}

impl From<&btleplug::api::Characteristic> for Characteristic {
    fn from(characteristic: &btleplug::api::Characteristic) -> Self {
        let uuid = characteristic.uuid.to_string();
        let service_uuid = characteristic.service_uuid.to_string();
        let properties = CharacteristicPropertiesFlags::from(characteristic.properties);

        Self {
            uuid,
            service_uuid,
            properties,
        }
    }
}

impl ToJsObject for Characteristic {
    fn to_object<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsObject> {
        let res = cx.empty_object();

        let uuid = cx.string(self.uuid.clone());
        res.set(cx, "uuid", uuid)?;

        let service_uuid = cx.string(self.service_uuid.clone());
        res.set(cx, "service_uuid", service_uuid)?;

        let properties = self.properties.to_object(cx)?;
        res.set(cx, "properties", properties)?;

        Ok(res)
    }
}

impl<T> ToJsArray for Vec<T>
where
    T: ToJsObject,
{
    fn to_array<'a>(&self, cx: &mut impl Context<'a>) -> JsResult<'a, JsArray> {
        let res = JsArray::new(cx, self.len() as u32);
        cx.empty_array();

        for (idx, val) in self.iter().enumerate() {
            let arr_val = val.to_object(cx)?;
            res.set(cx, idx as u32, arr_val)?;
        }

        Ok(res)
    }
}
