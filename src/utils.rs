use crate::model::{Peripheral, ToJsObject};
use btleplug::api::{CentralEvent, Peripheral as _};
use neon::prelude::{Context, JsObject, JsResult, Object};
use std::env::consts::OS;

/// This is used to parse MAC address on Windows and Linux
/// and UUID on MacOS.
pub fn get_uuid_from_peripheral(p: &btleplug::platform::Peripheral) -> String {
    // For Linux and Windows use MAC address
    match OS {
        "linux" | "windows" => p.address().to_string().replace(":", "").to_lowercase(),
        "macos" => {
            // Its in format PeripheralId(<id>)
            let uuid = format!("{:?}", p.id());
            uuid.replace("PeripheralId(", "")
                .replace(")", "")
                .to_lowercase()
        }
        _ => {
            format!("Not supported platform")
        }
    }
}

fn find_peripheral(
    peripherals: Option<Vec<Peripheral>>,
    id: Option<btleplug::platform::PeripheralId>,
) -> Option<Peripheral> {
    if peripherals.is_none() || id.is_none() {
        return None;
    }

    let peripherals = peripherals.unwrap();
    let id = id.unwrap();

    peripherals.iter().find(|p| p.peripheral_id == id).cloned()
}

pub fn central_event_to_js_event<'a>(
    cx: &mut impl Context<'a>,
    event: Option<CentralEvent>,
    peripherals: Option<Vec<Peripheral>>,
) -> JsResult<'a, JsObject> {
    let res = cx.empty_object();

    if event.is_none() {
        let typ = cx.string("unknown");
        res.set(cx, "type", typ)?;

        return Ok(res);
    }

    let event = event.unwrap();

    // Specify properties
    let (typ, id) = match event {
        CentralEvent::DeviceConnected(id) => ("connect", Some(id)),
        CentralEvent::DeviceDiscovered(id) => ("discover", Some(id)),
        CentralEvent::DeviceDisconnected(id) => ("disconnect", Some(id)),
        _ => ("unknown", None),
    };

    let peripheral = find_peripheral(peripherals, id);
    let typ = cx.string(typ);

    // Feed them into object
    res.set(cx, "type", typ)?;
    if let Some(p) = peripheral {
        let per = p.to_object(cx)?;
        res.set(cx, "peripheral", per)?;
    }

    Ok(res)
}
