use btleplug::api::ValueNotification;
use btleplug::api::{Central, CentralEvent, Manager as _, Peripheral as _, ScanFilter};
use btleplug::platform::{Adapter, Manager};
use futures::stream::Stream;
use futures::stream::StreamExt;
use std::collections::{BTreeSet, HashMap};
use std::pin::Pin;
use std::sync::atomic::{AtomicBool, Ordering};
use tokio::sync::{Mutex, RwLock};

use crate::error::LibraryError;
use crate::model::{Characteristic, Peripheral, Service};
use crate::utils::get_uuid_from_peripheral;

pub type Result<T> = std::result::Result<T, LibraryError>;

#[allow(dead_code)]
pub struct BleModule {
    pub adapter: RwLock<Option<Adapter>>,
    manager: RwLock<Option<Manager>>,
    initialized: AtomicBool,
    connected: Mutex<Option<btleplug::platform::Peripheral>>,
    notifications: Mutex<Option<Pin<Box<dyn Stream<Item = ValueNotification> + Send>>>>,

    // Persistent information about peripherals
    peripherals: RwLock<Vec<btleplug::platform::Peripheral>>,
    services: RwLock<HashMap<String, BTreeSet<btleplug::api::Service>>>,
    characteristics: RwLock<HashMap<String, BTreeSet<btleplug::api::Characteristic>>>,
}

#[allow(dead_code)]
impl BleModule {
    pub fn new() -> Self {
        Self {
            adapter: RwLock::new(None),
            manager: RwLock::new(None),
            initialized: AtomicBool::new(false),
            connected: Mutex::new(None),
            peripherals: RwLock::new(vec![]),
            notifications: Mutex::new(None),
            services: RwLock::new(HashMap::new()),
            characteristics: RwLock::new(HashMap::new()),
        }
    }

    pub async fn init(&self) -> Result<()> {
        if self.initialized.load(Ordering::SeqCst) {
            return Ok(());
        }

        // Find some adapter
        let manager = Manager::new().await?;
        let adapters = manager.adapters().await?;

        // We will always select the first adapter :/
        let adapter = match adapters.into_iter().next() {
            Some(a) => a,
            None => return Err(LibraryError::new_bt("No available adapters")),
        };

        let mut mng = self.manager.write().await;
        mng.replace(manager);
        let mut adp = self.adapter.write().await;
        adp.replace(adapter);

        self.initialized.swap(true, Ordering::Relaxed);
        Ok(())
    }

    pub async fn receive_event(&self) -> Result<Option<CentralEvent>> {
        let adapter = self.adapter.read().await;
        if adapter.is_none() {
            return Err(LibraryError::new_other(
                "No adapter was initialized! Try to init library first...",
            ));
        }

        let mut stream = adapter.as_ref().unwrap().events().await?;
        Ok(stream.next().await.clone())
    }

    pub async fn start_scan(&self) -> Result<()> {
        let adapter = self.adapter.read().await;
        if adapter.is_none() {
            return Err(LibraryError::new_other(
                "No adapter was initialized! Try to init library first...",
            ));
        }

        Ok(adapter
            .as_ref()
            .unwrap()
            .start_scan(ScanFilter::default())
            .await?)
    }

    pub async fn stop_scan(&self) -> Result<()> {
        let adapter = self.adapter.read().await;
        if adapter.is_none() {
            return Err(LibraryError::new_other(
                "No adapter was initialized! Try to init library first...",
            ));
        }

        Ok(adapter.as_ref().unwrap().stop_scan().await?)
    }

    async fn fetch_peripherals(&self) -> Result<()> {
        let adapter = self.adapter.read().await;
        if adapter.is_none() {
            return Err(LibraryError::new_other(
                "No adapter was initialized! Try to init library first...",
            ));
        }

        let mut peripherals = self.peripherals.write().await;
        *peripherals = adapter.as_ref().unwrap().peripherals().await?;

        Ok(())
    }

    async fn get_peripheral(&self, id: &str) -> Option<btleplug::platform::Peripheral> {
        let peripherals = self.peripherals.read().await;

        let peripheral = peripherals
            .iter()
            .find(|p| id.cmp(&get_uuid_from_peripheral(p)).is_eq());
        if let Some(p) = peripheral {
            Some(p.clone())
        } else {
            None
        }
    }

    async fn replace_peripheral(&self, peripheral: btleplug::platform::Peripheral) -> () {
        let id = peripheral.id();
        let mut peripherals = self.peripherals.write().await;
        if let Some(idx) = peripherals.iter().position(|p| p.id() == id) {
            peripherals.swap_remove(idx);
        }
        peripherals.push(peripheral);
    }

    pub async fn peripherals(&self) -> Result<Vec<Peripheral>> {
        self.fetch_peripherals().await?;

        let peripherals = self.peripherals.read().await;
        let mut result: Vec<Peripheral> = Vec::with_capacity(peripherals.len());
        for p in peripherals.iter() {
            let per = Peripheral::from(p).await;
            result.push(per);
        }

        Ok(result)
    }

    pub async fn connect(&self, id: &str) -> Result<bool> {
        // Check for already connected devices
        let mut conn = self.connected.lock().await;
        if conn.is_some() {
            return Err(LibraryError::new_other(
                "Already connected to another device!",
            ));
        }

        let peripheral = if let Some(p) = self.get_peripheral(id).await {
            p
        } else {
            return Ok(false);
        };

        for _ in 0..3 {
            let res = peripheral.connect().await;
            if res.is_ok() {
                break;
            }

            // Check for le-local-disconnect error
            let err = res.err().unwrap();
            if let btleplug::Error::Other(_) = err {
                tokio::time::sleep(tokio::time::Duration::from_millis(500)).await;
                continue;
            }

            return Err(err.into());
        }

        if peripheral.is_connected().await.is_err() {
            return Err(LibraryError::new_other(
                "Failed to connect with unknown reason",
            ));
        }

        // Now we are connected, so save information about it
        conn.replace(peripheral);

        Ok(true)
    }

    pub async fn disconnect(&self) -> Result<()> {
        // Check for already connected devices
        let mut conn = self.connected.lock().await;
        if conn.is_none() {
            return Err(LibraryError::new_other("No connected device!"));
        }
        let per = conn.take().unwrap();
        per.disconnect().await?;

        Ok(())
    }

    pub async fn discover(&self, id: &str) -> Result<()> {
        let peripheral = if let Some(p) = self.get_peripheral(id).await {
            p
        } else {
            return Err(LibraryError::new_other("No peripheral found!"));
        };

        // Discover all services and their characteristics
        peripheral.discover_services().await?;

        // Read services and characteristics and persist them
        let services = peripheral.services();
        let characteristics = peripheral.characteristics();
        let key = get_uuid_from_peripheral(&peripheral);

        let mut srvs = self.services.write().await;
        srvs.insert(key.clone(), services);
        drop(srvs);

        let mut chars = self.characteristics.write().await;
        chars.insert(key, characteristics);
        drop(chars);

        self.replace_peripheral(peripheral).await;
        Ok(())
    }

    pub async fn services(&self, id: &str) -> Result<Vec<Service>> {
        let map = self.services.read().await;
        let services = map.get(id);
        if services.is_none() {
            return Err(LibraryError::new_other(
                "Call discover before calling services",
            ));
        }
        let services: Vec<Service> = services
            .as_ref()
            .unwrap()
            .iter()
            .map(|s| Service::from(s))
            .collect();

        Ok(services)
    }

    pub async fn characteristics(&self, id: &str) -> Result<Vec<Characteristic>> {
        let map = self.characteristics.read().await;
        let characteristics = map.get(id);
        if characteristics.is_none() {
            return Err(LibraryError::new_other(
                "Call discover before calling characteristics",
            ));
        }
        let characteristics: Vec<Characteristic> = characteristics
            .as_ref()
            .unwrap()
            .iter()
            .map(|ch| Characteristic::from(ch))
            .collect();

        Ok(characteristics)
    }

    pub async fn write(
        &self,
        id: &str,
        char: &str,
        data: &[u8],
        write_type: btleplug::api::WriteType,
    ) -> Result<()> {
        // Check if we are connected...
        let peripheral = if let Some(p) = self.get_peripheral(id).await {
            p
        } else {
            return Err(LibraryError::new_other("No peripheral found!"));
        };

        // Discover services and characteristics
        peripheral.discover_services().await?;

        let chars = peripheral.characteristics();
        let char = if let Some(ch) = chars
            .iter()
            .find(|ch| char.cmp(&ch.uuid.to_string()).is_eq())
        {
            ch
        } else {
            return Err(LibraryError::new_other(
                "Unable to call 'write' function on non-existing characteristic",
            ));
        };

        Ok(peripheral.write(char, data, write_type).await?)
    }

    pub async fn notify(&self, id: &str, char: &str) -> Result<()> {
        let peripheral = if let Some(p) = self.get_peripheral(id).await {
            p
        } else {
            return Err(LibraryError::new_other("No peripheral found!"));
        };

        // Discover services and characteristics
        peripheral.discover_services().await?;

        let chars = peripheral.characteristics();
        let char = if let Some(ch) = chars
            .iter()
            .find(|ch| char.cmp(&ch.uuid.to_string()).is_eq())
        {
            ch
        } else {
            return Err(LibraryError::new_other(
                "Unable to call 'write' function on non-existing characteristic",
            ));
        };

        peripheral.subscribe(char).await?;

        let mut notifications = self.notifications.lock().await;
        *notifications = Some(peripheral.notifications().await?);

        Ok(())
    }

    pub async fn receive_data(&self) -> Result<Option<ValueNotification>> {
        let mut notifications = self.notifications.lock().await;
        if notifications.is_none() {
            return Err(LibraryError::new_other("No notifications registered!"));
        }
        let mut stream = notifications.take().unwrap();
        drop(notifications);

        let value = stream.next().await;
        let mut notifications = self.notifications.lock().await;
        *notifications = Some(stream);

        Ok(value)
    }
}
