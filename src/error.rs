use std::convert::From;
use std::fmt;

#[derive(Debug)]
pub enum LibraryError {
    Bluetooth(String),
    Other(String),
}

impl LibraryError {
    pub fn new_bt(msg: &str) -> Self {
        Self::Bluetooth(format!("{}", msg))
    }

    pub fn new_other(msg: &str) -> Self {
        Self::Other(format!("{}", msg))
    }
}

impl fmt::Display for LibraryError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LibraryError::Bluetooth(msg) => write!(f, "Bluetooth error: {}", msg),
            LibraryError::Other(msg) => write!(f, "Other error: {}", msg),
        }
    }
}

impl From<btleplug::Error> for LibraryError {
    fn from(e: btleplug::Error) -> Self {
        Self::Bluetooth(e.to_string())
    }
}

impl<T> From<std::sync::PoisonError<T>> for LibraryError {
    fn from(e: std::sync::PoisonError<T>) -> Self {
        Self::Other(e.to_string())
    }
}
