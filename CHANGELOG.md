# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2022-05-17

### Added
 - Scanning, reading of Peripherals and interaction with Characteristics
 - Basic events of discovered, disconnected and connected device
 - Some structure of the bindings from Rust to JavaScript/TypeScript

[unreleased]: https://gitlab.com/nevim42/ruble/compare/v0.1.0...sandbox
[0.1.0]: https://gitlab.com/nevim42/ruble/commits/v0.1.0
