/* eslint-disable */
import { ruble, WriteType, EventType, Peripheral } from '../lib';
import { performance } from 'perf_hooks';

const MAX_BLE_CHUNK_SIZE = 10;

/**
 * Promisify setTimeout funcion
 */
const delay = (sec: number): Promise<void> => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), sec * 1000);
  });
};

const yellow = '\x1b[33m';
const reset = '\x1b[0m';
const blue = '\x1b[34m';
const green = '\x1b[32m';

const timers: {
  [name: string]: number;
} = {};

const before = (name: string) => {
  timers[name] = performance.now();
  console.log(`\nBefore calling ${name}()`);
};

const after = (name: string) => {
  const end = performance.now();
  const start = timers[name] ? timers[name] : end;
  const diff = end - start;
  const ms = Math.round(diff * 10) / 10;

  let diffMsg = '';
  if (ms > 0) {
    diffMsg = ` ${yellow}+${ms}ms${reset}`;
  }

  console.log(`After calling ${name}()${diffMsg}`);
};

const printPeripheral = (p: Peripheral): string => {
  return `{ id: ${p.id}, name: ${p.name} }`;
};

const splitPacketIntoChunks = (packet: Buffer): Buffer[] => {
  // Check for only one packet
  if (packet.length <= MAX_BLE_CHUNK_SIZE) {
    return [packet];
  }

  const chunks: Buffer[] = [];

  while (packet.length > 0) {
    const cut = Math.min(packet.length, MAX_BLE_CHUNK_SIZE);
    const curr = packet.slice(0, cut);
    chunks.push(curr);

    // eslint-disable-next-line no-param-reassign
    packet = packet.slice(cut, packet.length);
  }

  return chunks;
};

/**
 * Entry point of this test
 */
const main = async () => {
  before('ruble.init');
  await ruble.init();
  after('ruble.init');

  const found: string[] = [];
  const cb = (p: Peripheral): void => {
    if (found.includes(p.id)) return;
    found.push(p.id);
    console.log(`${blue}Discovered: ${printPeripheral(p)}${reset}`);
  };
  before('subscribe');
  ruble.on(EventType.DeviceDiscovered, cb);
  after('subscribe');

  before('ruble.startScan');
  await ruble.startScan();
  after('ruble.startScan');

  before('delay');
  await delay(2);
  after('delay');

  before('ruble.stopScan');
  await ruble.stopScan();
  after('ruble.stopScan');

  before('ruble.peripherals');
  const peripherals = await ruble.peripherals();
  after('ruble.peripherals');

  // before('mapping peripherals');
  const force_name = /force/i;
  // const printMe = await Promise.all(
  //   peripherals.map(async (p) => {
  //     if (!force_name.test(p.name)) {
  //       return { id: p.id, name: p.name, services: [], chars: [] };
  //     }
  //
  //     await p.connect();
  //     await p.discover();
  //     const services = await p.services();
  //     const characteristics = await p.characteristics();
  //     await p.disconnect();
  //
  //     return {
  //       id: p.id,
  //       name: p.name,
  //       services: services.map(({ uuid }) => uuid),
  //       chars: characteristics.map(({ uuid }) => uuid),
  //     };
  //   })
  // );
  // after('mapping peripherals');
  //
  // console.log('\nPeripherals:', printMe);

  // '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
  const force = peripherals.find(({ name }) => force_name.test(name));
  if (!force) return;

  before('peripheral.connect');
  await force.connect();
  after('peripheral.connect');

  await force.discover();
  const chars = await force.characteristics();
  const rxChar = chars.find(({ uuid }) => uuid === '6e400002-b5a3-f393-e0a9-e50e24dcca9e');
  const txChar = chars.find(({ uuid }) => uuid === '6e400003-b5a3-f393-e0a9-e50e24dcca9e');

  console.log(rxChar, txChar);
  if (!rxChar || !txChar) return;

  before('characteristics.notify');
  await txChar.notify();
  after('characteristics.notify');

  before('onData');
  const data_clb = (data: Buffer) => {
    console.log('Received:', data);
  };
  ruble.on(EventType.Data, data_clb);
  after('onData');

  before('characteristics.write');
  const connectMsg = Buffer.from([
    0x7e, 0x00, 0xc0, 0x02, 0x00, 0x16, 0x00, 0x01, 0x00, 0x44, 0x69, 0x76, 0x65, 0x53, 0x6f, 0x66, 0x74, 0x20, 0x41,
    0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0xc3, 0x97, 0x7e,
  ]);

  const chunks = splitPacketIntoChunks(connectMsg);
  for (const chunk of chunks) {
    const data = Array.prototype.slice.call(chunk, 0);
    console.log('Sending:', data);
    await rxChar.write(data, WriteType.WithoutResponse);
  }
  after('characteristics.write');

  before('delay');
  await delay(2);
  after('delay');

  before('peripheral.disconnect');
  await force.disconnect();
  after('peripheral.disconnect');

  // console.log(
  //   '\nPeripherals:',
  //   peripherals.map(({ uuid, name }) => name)
  // );

  return Promise.resolve();
};

// Just call an entry point
main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
