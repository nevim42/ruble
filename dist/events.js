"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventsFactory = void 0;
// eslint-disable-next-line camelcase
var rust_1 = require("../dist/rust");
var convertRustEventType = function (event) {
    switch (event) {
        case "connect" /* DeviceConnected */:
            return "connect" /* DeviceConnected */;
        case "disconnect" /* DeviceDisconnected */:
            return "disconnect" /* DeviceDisconnected */;
        case "discover" /* DeviceDiscovered */:
            return "discover" /* DeviceDiscovered */;
        case "error" /* Error */:
            return "error" /* Error */;
        default:
            return "unknown" /* Unknown */;
    }
};
/**
 * Factory of event system for events from the Rust library.
 */
var eventsFactory = function () {
    var _a, _b;
    // Flag if we are handling active stream of data
    var _dataStreamActive = false;
    var _peripheralMapper;
    // Storage for all event handlers
    var _handlers = (_a = {},
        _a["data" /* Data */] = [],
        _a["connect" /* DeviceConnected */] = [],
        _a["disconnect" /* DeviceDisconnected */] = [],
        _a["discover" /* DeviceDiscovered */] = [],
        _a["error" /* Error */] = [],
        _a["unknown" /* Unknown */] = [],
        _a);
    // Storage for all event handlers that will be used only once
    var _onceHandlers = (_b = {},
        _b["data" /* Data */] = [],
        _b["connect" /* DeviceConnected */] = [],
        _b["disconnect" /* DeviceDisconnected */] = [],
        _b["discover" /* DeviceDiscovered */] = [],
        _b["error" /* Error */] = [],
        _b["unknown" /* Unknown */] = [],
        _b);
    var _initEvents = function () {
        var callback = function (e) {
            // Register next catcher
            (0, rust_1.catch_event)(callback);
            var event = convertRustEventType(e.type);
            if (event === "unknown" /* Unknown */)
                return _fireEvent(event);
            if (event === "error" /* Error */)
                return _fireEvent(event, e.message);
            if (event === "discover" /* DeviceDiscovered */ ||
                event === "connect" /* DeviceConnected */ ||
                event === "disconnect" /* DeviceDisconnected */) {
                var peripheral = e.peripheral;
                if (peripheral) {
                    _fireEvent(event, _peripheralMapper(peripheral));
                }
            }
            // Do nothing, some unknown event
        };
        (0, rust_1.catch_event)(callback);
    };
    var _initData = function () {
        var callback = function (data) {
            // No more data, stream is closed in the Rust side
            if (!data) {
                _dataStreamActive = false;
                removeAll("data" /* Data */);
                return;
            }
            (0, rust_1.catch_data)(callback);
            _fireEvent("data" /* Data */, Buffer.from(data));
        };
        (0, rust_1.catch_data)(callback);
    };
    var init = function (peripheralMapper) {
        _peripheralMapper = peripheralMapper;
        _initEvents();
    };
    var _activateDataStream = function (event) {
        if (event !== "data" /* Data */)
            return;
        if (_dataStreamActive)
            return;
        _initData();
    };
    var _fireEvent = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        _handlers[event].forEach(function (handler) {
            handler.apply(void 0, args);
        });
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        _onceHandlers[event].forEach(function (handler) {
            handler.apply(void 0, args);
        });
        _onceHandlers[event] = [];
    };
    var on = function (event, handler) {
        _activateDataStream(event);
        _handlers[event].push(handler);
        return function () { return removeOn(event, handler); };
    };
    var removeOn = function (event, handler) {
        _handlers[event] = _handlers[event].filter(function (hndl) { return hndl !== handler; });
    };
    var once = function (event, handler) {
        _activateDataStream(event);
        _onceHandlers[event].push(handler);
        return function () { return removeOnce(event, handler); };
    };
    var removeOnce = function (event, handler) {
        _onceHandlers[event] = _onceHandlers[event].filter(function (hndl) { return hndl !== handler; });
    };
    var removeAll = function (event) {
        _handlers[event] = [];
        _onceHandlers[event] = [];
    };
    return {
        init: init,
        on: on,
        once: once,
        removeAll: removeAll,
        removeOn: removeOn,
        removeOnce: removeOnce,
    };
};
exports.eventsFactory = eventsFactory;
//# sourceMappingURL=events.js.map