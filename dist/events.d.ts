/// <reference types="node" />
import type { Peripheral as RustPeripheral } from '../dist/rust';
import type { Peripheral } from './interfaces';
export declare const enum EventType {
    Unknown = "unknown",
    DeviceDiscovered = "discover",
    DeviceConnected = "connect",
    DeviceDisconnected = "disconnect",
    Error = "error",
    Data = "data"
}
export declare type ConnectHandler = (peripheral: Peripheral) => void;
export declare type DataHandler = (data: Buffer) => void;
export declare type DisconnectHandler = (peripheral: Peripheral) => void;
export declare type DiscoverHandler = (peripheral: Peripheral) => void;
export declare type ErrorHandler = (msg: string) => void;
export declare type UnknownHandler = () => void;
export declare type EventHandler = {
    [EventType.Data]: DataHandler;
    [EventType.DeviceConnected]: ConnectHandler;
    [EventType.DeviceDisconnected]: DiscoverHandler;
    [EventType.DeviceDiscovered]: DiscoverHandler;
    [EventType.Error]: ErrorHandler;
    [EventType.Unknown]: UnknownHandler;
};
export declare type EventHandlers = {
    [event in EventType]: EventHandler[event][];
};
export declare type EventUnsubscriber = () => void;
export declare type AddHandler = <Event extends EventType>(event: Event, handler: EventHandler[Event]) => EventUnsubscriber;
export declare type RemoveHandler = <Event extends EventType>(event: Event, handler: EventHandler[Event]) => void;
/**
 * Factory of event system for events from the Rust library.
 */
export declare const eventsFactory: () => {
    init: (peripheralMapper: (peripheral: RustPeripheral) => Peripheral) => void;
    on: <Event extends EventType>(event: Event, handler: EventHandler[Event]) => EventUnsubscriber;
    once: <Event_1 extends EventType>(event: Event_1, handler: EventHandler[Event_1]) => EventUnsubscriber;
    removeAll: (event: EventType) => void;
    removeOn: <Event_2 extends EventType>(event: Event_2, handler: EventHandler[Event_2]) => void;
    removeOnce: <Event_3 extends EventType>(event: Event_3, handler: EventHandler[Event_3]) => void;
};
