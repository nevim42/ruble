export type PeripheralId = string;
export type Uuid = string;

export interface Peripheral {
  id: string;
  name: string;
}

export interface Service {
  uuid: Uuid;
  primary: boolean;
}

export interface CharacteristicPropertiesFlags {
  broadcast: boolean;
  read: boolean;
  write_without_response: boolean;
  write: boolean;
  notify: boolean;
  indicate: boolean;
  authenticated_signed_writes: boolean;
  extended_properties: boolean;
}

export interface Characteristic {
  uuid: Uuid;
  serviceUuid: Uuid;
  properties: CharacteristicPropertiesFlags;
}

export const enum WriteType {
  WithoutResponse = 0,
  WithResponse = 1,
}

export const enum EventType {
  DeviceConnected = 'connect',
  DeviceDisconnected = 'disconnect',
  DeviceDiscovered = 'discover',
  Error = 'error',
  Unknown = 'unknown',
}

export interface Event {
  type: EventType;
  peripheral?: Peripheral;
  message?: string;
}

// Methods on adapter
export declare function init(): Promise<void>;
export declare function start_scan(): Promise<void>;
export declare function stop_scan(): Promise<void>;
export declare function peripherals(): Promise<Peripheral[]>;

// Methods on peripheral
export declare function connect(id: PeripheralId): Promise<boolean>;
export declare function disconnect(id: PeripheralId): Promise<void>;
export declare function discover(id: PeripheralId): Promise<void>;
export declare function services(id: PeripheralId): Promise<Service[]>;
export declare function characteristics(id: PeripheralId): Promise<Characteristic[]>;

// Methods on characteristic
export declare function notify(id: PeripheralId, char: Uuid): Promise<void>;
export declare function write(id: PeripheralId, char: Uuid, data: number[], type: WriteType): Promise<void>;

// All types of event streams
export declare function catch_event(callback: (e: Event) => void): void;
export declare function catch_data(callback: (data: number[]) => void): void;
