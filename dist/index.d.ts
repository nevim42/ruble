import type { RuBLE } from './interfaces';
export { Characteristic, Peripheral, RuBLE, Service } from './interfaces';
export { ConnectHandler, DataHandler, DisconnectHandler, DiscoverHandler, ErrorHandler, EventHandler, EventType, EventUnsubscriber, UnknownHandler, } from './events';
export { PeripheralId, Uuid, WriteType } from './rust';
export declare const ruble: RuBLE;
