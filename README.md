# RuBLE for NodeJS
[![DeepSource][DS_Badge]](https://deepsource.io/gl/nevim42/ruble/)

NodeJS binding for BLE (Bluetooth Low Energy) library written in Rust.
It uses Neon to create bindings between Rust and NodeJS.

## Roadmap

Functions:
 - [X] Init BLE and setup adapter
 - [X] Scanning manipulation
 - [X] Get found devices
 - [X] Read services and characteristics of device
 - [X] Connect/disconnect devices
 - [X] Write to the characteristic
 - [X] Listen for data change in characteristic

Events:
 - [X] Connect and disconnect events
 - [X] Device discover event
 - [ ] Start/Stop scanning event (Not possible?)
 - [ ] Error/Warning events

State:
 - [ ] Is scanning (Not possible?)
 - [ ] Is available
 - [X] Is connected
 - [X] Connected device

[DS_Badge]: https://deepsource.io/gl/nevim42/ruble.svg/?label=active+issues&show_trend=true&token=gb7_SD80jCMf1yOE47t8om1I
