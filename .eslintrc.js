module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'standard',
    'plugin:jest/recommended',
    'prettier',
    'plugin:import/recommended',
    'plugin:import/typescript',
  ],
  plugins: ['@typescript-eslint', 'jest', 'prettier', 'import'],
  env: {
    es2021: true,
    jest: true,
    node: true,
  },
  globals: {
    NodeJS: 'readonly',
  },
  root: true,
  rules: {
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/consistent-type-imports': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/no-var-requires': 'off',
    'comma-dangle': 'off',
    'import/no-duplicates': 'error',
    'max-len': ['error', { code: 120 }],
    'no-console': 'error',
    'no-duplicate-imports': 'off',
    'no-extra-semi': 'error',
    'no-undef': 'off',
    'no-unused-vars': 'off',
    'no-use-before-define': 'off',
    'prettier/prettier': 'error',
    'sort-imports': 'off',
    'space-before-function-paren': 'off',
    eqeqeq: 'error',
    semi: ['error', 'always'],
  },
  overrides: [
    {
      files: ['*.js', '*.ts'],
    },
  ],
  settings: {
    'import/resolver': {
      alias: {
        map: [['../dist', './dist']],
        extensions: ['.node'],
      },
    },

  },
};
