import type {
  Characteristic as RustCharacteristic,
  Peripheral as RustPeripheral,
  PeripheralId,
  Service as RustService,
} from '../dist/rust';
import type { AddHandler, EventType, RemoveHandler } from './events';

export interface Peripheral extends RustPeripheral {
  connect: () => Promise<boolean>;
  disconnect: () => Promise<void>;

  // Services & Characteristics
  discover: () => Promise<void>;
  services: () => Promise<Service[]>;
  characteristics: () => Promise<Characteristic[]>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Service extends RustService {}

export interface Characteristic extends RustCharacteristic {
  notify: () => Promise<void>; // Enable notifications of data
  write: (data: Buffer, type: number) => Promise<void>;
}

export interface RuBLE {
  // Control methods
  init: () => Promise<void>;
  startScan: () => Promise<void>;
  stopScan: () => Promise<void>;

  // Peripherals
  connect: (id: PeripheralId) => Promise<boolean>;
  disconnect: (id: PeripheralId) => Promise<void>;
  peripherals: () => Promise<Peripheral[]>;

  // Events
  on: AddHandler;
  once: AddHandler;
  removeAll: (event: EventType) => void;
  removeOn: RemoveHandler;
  removeOnce: RemoveHandler;
}
