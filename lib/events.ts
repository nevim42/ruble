// eslint-disable-next-line camelcase
import { catch_event, catch_data, EventType as RustEventType } from '../dist/rust';
import type { Event as RustEvent, Peripheral as RustPeripheral } from '../dist/rust';
import type { Peripheral } from './interfaces';

export const enum EventType {
  Unknown = 'unknown',
  DeviceDiscovered = 'discover',
  DeviceConnected = 'connect',
  DeviceDisconnected = 'disconnect',
  Error = 'error',
  Data = 'data',
}

const convertRustEventType = (event: RustEventType): EventType => {
  switch (event) {
    case RustEventType.DeviceConnected:
      return EventType.DeviceConnected;
    case RustEventType.DeviceDisconnected:
      return EventType.DeviceDisconnected;
    case RustEventType.DeviceDiscovered:
      return EventType.DeviceDiscovered;
    case RustEventType.Error:
      return EventType.Error;
    default:
      return EventType.Unknown;
  }
};

export type ConnectHandler = (peripheral: Peripheral) => void;
export type DataHandler = (data: Buffer) => void;
export type DisconnectHandler = (peripheral: Peripheral) => void;
export type DiscoverHandler = (peripheral: Peripheral) => void;
export type ErrorHandler = (msg: string) => void;
export type UnknownHandler = () => void;

export type EventHandler = {
  [EventType.Data]: DataHandler;
  [EventType.DeviceConnected]: ConnectHandler;
  [EventType.DeviceDisconnected]: DiscoverHandler;
  [EventType.DeviceDiscovered]: DiscoverHandler;
  [EventType.Error]: ErrorHandler;
  [EventType.Unknown]: UnknownHandler;
};

export type EventHandlers = {
  [event in EventType]: EventHandler[event][];
};

export type EventUnsubscriber = () => void;

export type AddHandler = <Event extends EventType>(event: Event, handler: EventHandler[Event]) => EventUnsubscriber;
export type RemoveHandler = <Event extends EventType>(event: Event, handler: EventHandler[Event]) => void;

/**
 * Factory of event system for events from the Rust library.
 */
export const eventsFactory = () => {
  // Flag if we are handling active stream of data
  let _dataStreamActive = false;
  let _peripheralMapper: (peripheral: RustPeripheral) => Peripheral;

  // Storage for all event handlers
  const _handlers: EventHandlers = {
    [EventType.Data]: [],
    [EventType.DeviceConnected]: [],
    [EventType.DeviceDisconnected]: [],
    [EventType.DeviceDiscovered]: [],
    [EventType.Error]: [],
    [EventType.Unknown]: [],
  };

  // Storage for all event handlers that will be used only once
  const _onceHandlers: EventHandlers = {
    [EventType.Data]: [],
    [EventType.DeviceConnected]: [],
    [EventType.DeviceDisconnected]: [],
    [EventType.DeviceDiscovered]: [],
    [EventType.Error]: [],
    [EventType.Unknown]: [],
  };

  const _initEvents = (): void => {
    const callback = (e: RustEvent): void => {
      // Register next catcher
      catch_event(callback);

      const event: EventType = convertRustEventType(e.type);
      if (event === EventType.Unknown) return _fireEvent(event);
      if (event === EventType.Error) return _fireEvent(event, e.message);
      if (
        event === EventType.DeviceDiscovered ||
        event === EventType.DeviceConnected ||
        event === EventType.DeviceDisconnected
      ) {
        const peripheral: RustPeripheral = e.peripheral;
        if (peripheral) {
          _fireEvent(event, _peripheralMapper(peripheral));
        }
      }

      // Do nothing, some unknown event
    };

    catch_event(callback);
  };

  const _initData = (): void => {
    const callback = (data: number[]): void => {
      // No more data, stream is closed in the Rust side
      if (!data) {
        _dataStreamActive = false;
        removeAll(EventType.Data);
        return;
      }

      catch_data(callback);
      _fireEvent(EventType.Data, Buffer.from(data));
    };

    catch_data(callback);
  };

  const init = (peripheralMapper: (peripheral: RustPeripheral) => Peripheral): void => {
    _peripheralMapper = peripheralMapper;
    _initEvents();
  };

  const _activateDataStream = (event: EventType): void => {
    if (event !== EventType.Data) return;
    if (_dataStreamActive) return;

    _initData();
  };

  const _fireEvent = <Event extends EventType>(event: Event, ...args: Parameters<EventHandler[Event]>): void => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _handlers[event].forEach((handler: (...args: any[]) => void) => {
      handler(...args);
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _onceHandlers[event].forEach((handler: (...args: any[]) => void) => {
      handler(...args);
    });
    _onceHandlers[event] = [];
  };

  const on = <Event extends EventType>(event: Event, handler: EventHandler[Event]): EventUnsubscriber => {
    _activateDataStream(event);
    _handlers[event].push(handler);
    return () => removeOn(event, handler);
  };

  const removeOn = <Event extends EventType>(event: Event, handler: EventHandler[Event]): void => {
    (_handlers[event] as EventHandler[Event][]) = _handlers[event].filter((hndl) => hndl !== handler);
  };

  const once = <Event extends EventType>(event: Event, handler: EventHandler[Event]): EventUnsubscriber => {
    _activateDataStream(event);
    _onceHandlers[event].push(handler);
    return () => removeOnce(event, handler);
  };

  const removeOnce = <Event extends EventType>(event: Event, handler: EventHandler[Event]): void => {
    (_onceHandlers[event] as EventHandler[Event][]) = _onceHandlers[event].filter((hndl) => hndl !== handler);
  };

  const removeAll = (event: EventType): void => {
    _handlers[event] = [];
    _onceHandlers[event] = [];
  };

  return {
    init,
    on,
    once,
    removeAll,
    removeOn,
    removeOnce,
  };
};
