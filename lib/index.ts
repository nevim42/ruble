/* eslint-disable camelcase */
import * as rust from '../dist/rust';
import type { Characteristic, Peripheral, RuBLE } from './interfaces';
import { eventsFactory } from './events';

// Re-export some stuff
export { Characteristic, Peripheral, RuBLE, Service } from './interfaces';
export {
  ConnectHandler,
  DataHandler,
  DisconnectHandler,
  DiscoverHandler,
  ErrorHandler,
  EventHandler,
  EventType,
  EventUnsubscriber,
  UnknownHandler,
} from './events';
export { PeripheralId, Uuid, WriteType } from '../dist/rust';

const mapRustPeripheralToJsPeripheral = (per: rust.Peripheral): Peripheral => {
  const { name, id } = per;

  const _chars = async (): Promise<Characteristic[]> => {
    const chars = await rust.characteristics(id);

    const res: Characteristic[] = chars.map(mapRustCharacteristicToJsCharacteristic(id));

    return res;
  };

  return {
    id,
    name,
    connect: () => rust.connect(id),
    disconnect: () => rust.disconnect(id),
    discover: () => rust.discover(id),
    services: () => rust.services(id),
    characteristics: _chars,
  };
};

const mapRustCharacteristicToJsCharacteristic =
  (id: rust.PeripheralId) =>
  (char: rust.Characteristic): Characteristic => {
    const write = async (data: Buffer, type: number): Promise<void> => {
      // Convert buffer to array of numbers
      const dataArray = Array.prototype.slice.call(data, 0);
      return rust.write(id, char.uuid, dataArray, type);
    };

    const notify = async (): Promise<void> => {
      return rust.notify(id, char.uuid);
    };

    return { ...char, write, notify };
  };

const rubleFactory = (): RuBLE => {
  const _peripherals = async (): Promise<Peripheral[]> => {
    const pers = await rust.peripherals();

    const res: Peripheral[] = pers.map(mapRustPeripheralToJsPeripheral);

    return res;
  };

  const _events = eventsFactory();

  const init = async () => {
    await rust.init();
    _events.init(mapRustPeripheralToJsPeripheral);
  };

  return {
    connect: rust.connect,
    disconnect: rust.disconnect,
    init,
    on: _events.on,
    once: _events.once,
    peripherals: _peripherals,
    removeAll: _events.removeAll,
    removeOn: _events.removeOn,
    removeOnce: _events.removeOnce,
    startScan: rust.start_scan,
    stopScan: rust.stop_scan,
  };
};

export const ruble = rubleFactory();
